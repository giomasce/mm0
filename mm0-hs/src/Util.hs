module Util where

import Control.Monad.Except
import Data.List (group, sort)
import System.Exit
import qualified Data.Map.Strict as M

fromJustError :: MonadError e m => e -> Maybe a -> m a
fromJustError errorval Nothing = throwError errorval
fromJustError _ (Just normalval) = return normalval

guardError :: MonadError e m => e -> Bool -> m ()
guardError _ True = return ()
guardError e _ = throwError e

insertNew :: (Ord k, MonadError e m) => e -> k -> v -> M.Map k v -> m (M.Map k v)
insertNew e k v m = do
  guardError e (M.notMember k m)
  return (M.insert k v m)

liftIO :: Either String a -> IO a
liftIO (Left e) = die e
liftIO (Right e) = return e

allUnique :: Ord a => [a] -> Bool
allUnique = all ((==) 1 . length) . group . sort

padL :: Int -> String -> String
padL n s
    | length s < n  = s ++ replicate (n - length s) ' '
    | otherwise = s

all2 :: (a -> b -> Bool) -> [a] -> [b] -> Bool
all2 r = go where
  go [] [] = True
  go (a:as) (b:bs) = r a b && go as bs
  go _ _ = False
