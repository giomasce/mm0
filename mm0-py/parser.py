#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys


class ParsingError(Exception):
    def __init__(self, message):
        super().__init__(message)


class Token(object):
    TYPE_SYMBOL = 0
    TYPE_IDENT = 1
    TYPE_NUM = 2
    TYPE_MATH = 3

    def __init__(self, type_, val):
        self.type = type_
        self.val = val

    def __str__(self):
        if self.type == self.TYPE_SYMBOL:
            return "<Symbol token: {}>".format(self.val)
        if self.type == self.TYPE_IDENT:
            return "<Identifier token: {}>".format(self.val)
        if self.type == self.TYPE_NUM:
            return "<Number token: {}>".format(self.val)
        if self.type == self.TYPE_MATH:
            return "<Math token: {}>".format(self.val)
        raise Exception("Should not arrive here")

    def __eq__(self, x):
        return self.type == x.type and self.val == x.val

    def decode(self, type_):
        if self.type == type_:
            return self.val
        else:
            return None

    def expect(self, type_):
        val = self.decode(type_)
        if val is None:
            raise ParsingError("Unexpected token type for '{}'".format(self))
        return val

    def expect_val(self, type_, val):
        val2 = self.expect(type_)
        if val2 != val:
            raise ParsingError("Unexpected token value for '{}'".format(self))


class Lexer(object):
    def __init__(self, fd):
        self.fd = fd
        self.buf = ''

    def read_white(self):
        if self.buf != '':
            tmp = self.buf
            self.buf = ''
            return tmp
        else:
            return self.fd.read(1)

    def read(self):
        while True:
            c = self.read_white()
            if c == ' ' or c == '\n':
                continue
            if c == '-':
                c = self.read_white()
                if c != '-':
                    raise ParsingError("Malformed comment")
                while True:
                    c = self.read_white()
                    if c == '\n':
                        break
                continue
            return c

    def get_token(self):
        token = self.get_token_()
        #print(token)
        return token

    def get_token_(self):
        c = self.read()
        if c == '':
            return None
        if c in set('*.:;()>{}='):
            return Token(Token.TYPE_SYMBOL, c)
        if c == '$':
            val = []
            while True:
                c = self.read_white()
                if c == '$':
                    break
                else:
                    val.append(c)
            return Token(Token.TYPE_MATH, "".join(val))
        vc = ord(c) - ord('0')
        if 0 <= vc <= 9:
            val = vc
            while True:
                c = self.read_white()
                vc = ord(c) - ord('0')
                if 0 <= vc <= 9:
                    if val == 0:
                        raise ParsingError("Malformed numeral (leading zero)")
                    val = val * 10 + vc
                else:
                    self.buf = c
                    break
            return Token(Token.TYPE_NUM, val)
        if c in set('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_'):
            val = [c]
            while True:
                c = self.read_white()
                if c in set('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789_'):
                    val.append(c)
                else:
                    self.buf = c
                    break
            return Token(Token.TYPE_IDENT, "".join(val))
        raise ParsingError("Illegal character {}".format(c))


class Sort(object):
    def __init__(self, name, pure, strict, provable, free):
        self.name = name
        self.pure = pure
        self.strict = strict
        self.provable = provable
        self.free = free


class SecParser(object):
    OP_TYPE_INFIXL = 1
    OP_TYPE_INFIXR = 2
    OP_TYPE_PREFIX = 3
    OP_TYPE_NOTATION = 4

    OP_TYPE_DICT = {
        'infixl': OP_TYPE_INFIXL,
        'infixr': OP_TYPE_INFIXR,
        'prefix': OP_TYPE_PREFIX,
    }

    def __init__(self):
        self.dels = set()
        self.ops = dict()

    def delimiter(self, dels):
        if len(self.ops) != 0:
            raise ParsingError("Cannot define delimiters once operators have been defined")
        expect_space = False
        for d in dels:
            if d in set(' \n'):
                expect_space = False
                continue
            if expect_space:
                raise ParsingError("Delimiters must be single characters")
            self.dels.add(d)
            expect_space = True

    def tokenize(self, formula):
        tokens = []
        tok = []
        for c in formula:
            if c in set(' \n') or c in self.dels:
                if len(tok) != 0:
                    tokens.append("".join(tok))
                    tok = []
                if c in self.dels:
                    tokens.append(c)
            else:
                tok.append(c)
        if len(tok) != 0:
            tokens.append("".join(tok))
        return tokens

    def define_op(self, type_, name, constant, prec):
        tokens = self.tokenize(constant)
        if len(tokens) != 1:
            raise ParsingError("Bad operator definition")
        [symb] = tokens
        if symb in self.ops:
            raise ParsingError("Operator defined twice")
        self.ops[symb] = (type_, prec, name)


class Parser(object):
    PREC_MAX = 2047

    def __init__(self, lexer):
        self.lexer = lexer
        self.ctxs = [{}]
        self.sorts = {}
        self.sec_parser = SecParser()

    def parse(self):
        while True:
            token = self.lexer.get_token()
            if token is None:
                break
            elif token == Token(Token.TYPE_SYMBOL, '{'):
                self.ctxs.append({})
            elif token == Token(Token.TYPE_SYMBOL, '}'):
                del self.ctxs[-1]
            else:
                ident = token.expect(Token.TYPE_IDENT)
                if self.parse_sort(ident):
                    pass
                elif self.parse_term_stmt_def(ident):
                    pass
                elif self.parse_delimiter(ident):
                    pass
                elif self.parse_operator(ident):
                    pass
                elif self.parse_coercion(ident):
                    pass
                else:
                    raise ParsingError("Unexpected token '{}'".format(ident))

    def parse_sort(self, ident):
        if ident not in set(['pure', 'strict', 'provable', 'free', 'sort']):
            return False
        pure = False
        strict = False
        provable = False
        free = False
        while ident != 'sort':
            if ident == 'pure':
                pure = True
            elif ident == 'strict':
                strict = True
            elif ident == 'provable':
                provable = True
            elif ident == 'free':
                free = True
            else:
                raise ParsingError("Illegal token while parsing sort")
            ident = self.lexer.get_token().expect(Token.TYPE_IDENT)
        name = self.lexer.get_token().expect(Token.TYPE_IDENT)
        sort = Sort(name, pure, strict, provable, free)
        if name in self.sorts:
            raise ParsingError("Sort name '{}' reused".format(name))
        self.sorts[name] = sort
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ';')
        return True

    def subparse_type_formula(self):
        token = self.lexer.get_token()
        type_ = token.decode(Token.TYPE_IDENT)
        formula = token.decode(Token.TYPE_MATH)
        if type_ is not None:
            type_ = [type_]
            while True:
                token = self.lexer.get_token()
                ident = token.decode(Token.TYPE_IDENT)
                symb = token.decode(Token.TYPE_SYMBOL)
                if ident is not None:
                    type_.append(ident)
                elif symb is not None:
                    break
                else:
                    raise ParsingError("Indentifier or symbol expected")
        elif formula is not None:
            symb = self.lexer.get_token().expect(Token.TYPE_SYMBOL)
        else:
            raise ParsingError("Type or formula expected")
        return (type_, formula, symb)

    def subparse_binder(self):
        symb = self.lexer.get_token().expect(Token.TYPE_SYMBOL)
        if symb == ':':
            return None
        if symb not in set('({'):
            raise ParsingError("Malformed binder (invalid opening character)")
        bound = symb == '{'
        vars_ = []
        while True:
            token = self.lexer.get_token()
            if token == Token(Token.TYPE_SYMBOL, ':'):
                break
            dummy = False
            if token == Token(Token.TYPE_SYMBOL, '.'):
                dummy = True
                token = self.lexer.get_token()
            name = token.expect(Token.TYPE_IDENT)
            vars_.append((dummy, name))
        type_, formula, symb = self.subparse_type_formula()
        if symb != ('}' if bound else ')'):
            raise ParsingError("Malformed binder (invalid closing character)")
        return (bound, vars_, type_, formula)

    def subparse_arrow(self):
        arrow = []
        while True:
            type_, formula, symb = self.subparse_type_formula()
            arrow.append((type_, formula))
            if symb != '>':
                return (arrow, symb)

    def resolve_vars(self, binders, arrow, can_infer, can_have_dummies):
        # Split all binders and create binders from the arrow
        new_binders = []
        for bound, vars_, type_, formula in binders:
            for dummy, var in vars_:
                new_binders.append((bound, dummy, var if var != '_' else '', type_, formula))
        for type_, formula in arrow:
            new_binders.append((False, False, '', type_, formula))

        # Check that all constraints are satisfied
        bounds = dict()
        regulars = dict()
        formulas = []
        for bound, dummy, var, type_, formula in new_binders:
            if type_ is not None:
                assert formula is None
                if len(formulas) != 0:
                    raise ParsingError("Cannot define a formula before a binding")
                if type_[0] not in self.sorts:
                    raise ParsingError("Unknown sort")
                sort = self.sorts[type_[0]]
                if var in bounds or var in regulars:
                    raise ParsingError("Variable defined twice")
                if bound:
                    bounds[var] = type_
                    # if len(regulars) != 0:
                    #     raise ParsingError("Cannot define a regular variable before a bound variable")
                    if len(type_) > 1:
                        raise ParsingError("Bound vars cannot have dependent type")
                    if sort.strict:
                        raise ParsingError("Bound variables cannot have a strict type")
                else:
                    regulars[var] = type_
                    if dummy:
                        raise ParsingError("Dummy variables must be bound")
                    for dep_var in type_[1:]:
                        if dep_var not in bounds:
                            raise ParsingError("Dependent variable is not bound")
            elif formula is not None:
                assert type_ is None
                if bound:
                    raise ParsingError("Formulae cannot be bound")
                if dummy:
                    raise ParsingError("Formulae cannot be dummy")
                # var can be defined, but is ignored (this means that
                # two formulas can have the same name, in theory)
                formulas.append(formula)
            else:
                assert False

        return bounds, regulars, formulas

    def parse_term_stmt_def(self, ident):
        if ident not in set(['term', 'axiom', 'theorem', 'def', 'notation']):
            return False
        name = self.lexer.get_token().expect(Token.TYPE_IDENT)
        binders = []
        while True:
            binder = self.subparse_binder()
            if binder is None:
                break
            binders.append(binder)
        arrow, symb = self.subparse_arrow()
        if ident in set(['def', 'notation']):
            if symb != '=':
                raise ParsingError("Malformed def/notation")
        else:
            if symb != ';':
                raise ParsingError("Malformed term/axiom/theorem")
        body = None
        if symb == '=':
            if ident == 'def':
                body = self.lexer.get_token().expect(Token.TYPE_MATH)
                self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ';')
            else:
                assert ident == 'notation'
                while True:
                    const, prec, ident, symb = self.subparse_prec_const()
                    if symb is not None:
                        if symb != ';':
                            raise ParsingError("Malformed notation (unexpected symbol)")
                        break
        self.resolve_vars(binders, arrow,
                          can_infer=ident in set(['axiom', 'theorem']),
                          can_have_dummies=ident == 'def')
        return True

    def subparse_prec(self):
        token = self.lexer.get_token()
        if token == Token(Token.TYPE_IDENT, 'max'):
            prec = self.PREC_MAX
        else:
            prec = token.expect(Token.TYPE_NUM)
            if prec >= self.PREC_MAX:
                raise ParsingError("Invalid precedence")
        return prec

    def subparse_prec_const(self):
        token = self.lexer.get_token()
        const = None
        prec = None
        ident = None
        symb = None
        if token == Token(Token.TYPE_SYMBOL, '('):
            const = self.lexer.get_token().expect(Token.TYPE_MATH)
            self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ':')
            prec = self.subparse_prec()
            self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ')')
        else:
            ident = token.decode(Token.TYPE_IDENT)
            symb = token.decode(Token.TYPE_SYMBOL)
            if ident is None and symb is None:
                raise ParsingError("Expected prec_constant, identifier or symbol")
        return const, prec, ident, symb

    def parse_delimiter(self, ident):
        if ident != 'delimiter':
            return False
        dels = self.lexer.get_token().expect(Token.TYPE_MATH)
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ';')
        self.sec_parser.delimiter(dels)
        return True

    def parse_operator(self, ident):
        if ident not in set(['infixl', 'infixr', 'prefix']):
            return False
        name = self.lexer.get_token().expect(Token.TYPE_IDENT)
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ':')
        constant = self.lexer.get_token().expect(Token.TYPE_MATH)
        self.lexer.get_token().expect_val(Token.TYPE_IDENT, 'prec')
        prec = self.subparse_prec()
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ';')
        self.sec_parser.define_op(SecParser.OP_TYPE_DICT[ident], name, constant, prec)
        return True

    def parse_coercion(self, ident):
        if ident != 'coercion':
            return False
        name = self.lexer.get_token().expect(Token.TYPE_IDENT)
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ':')
        coercing = self.lexer.get_token().expect(Token.TYPE_IDENT)
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, '>')
        coerced = self.lexer.get_token().expect(Token.TYPE_IDENT)
        self.lexer.get_token().expect_val(Token.TYPE_SYMBOL, ';')
        return True


def main():
    lexer = Lexer(sys.stdin)
    parser = Parser(lexer)
    parser.parse()


if __name__ == '__main__':
    main()
