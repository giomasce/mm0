delimiter $ ( ) ~ { } , $;
strict provable sort wff;
term imp (ph ps: wff): wff; infixr imp: $->$ prec 25;
term neg (ph: wff): wff; prefix neg: $~$ prec 40;

axiom ax_1 (ph ps: wff): $ ph -> ps -> ph $;
axiom ax_2 (ph ps ch: wff): $ (ph -> ps -> ch) -> (ph -> ps) -> ph -> ch $;
axiom ax_3 (ph ps: wff): $ (~ph -> ~ps) -> ps -> ph $;
axiom ax_mp (ph ps: wff): $ ph $ > $ ph -> ps $ > $ ps $;

def iff (ph ps: wff): wff = $ ~((ph -> ps) -> ~(ps -> ph)) $;
infixl iff: $<->$ prec 20;

def and (ph ps: wff): wff = $ ~(ph -> ~ps) $;
infixl and: $/\$ prec 20;

def or (ph ps: wff): wff = $ ~ph -> ps $;
infixl or: $\/$ prec 30;

def tru (.p: wff): wff = $ p <-> p $; prefix tru: $T.$ prec max;
def fal: wff = $ ~T. $; prefix fal: $F.$ prec max;

sort nat;
term al {x: nat} (ph: wff x): wff; prefix al: $A.$ prec 30;

def ex {x: nat} (ph: wff x): wff = $ ~(A. x ~ph) $;
prefix ex: $E.$ prec 30;

axiom ax_gen (ph: wff) {x: nat}: $ ph $ > $ A. x ph $;
axiom ax_4 {x: nat} (ph ps: wff x): $ A. x (ph -> ps) -> A. x ph -> A. x ps $;
axiom ax_5 {x: nat} (ph: wff): $ ph -> A. x ph $;

term eq (a b: nat): wff; infixl eq: $=$ prec 50;

def ne (a b: nat): wff = $ ~ a = b $; infixl ne: $!=$ prec 50;

def sb (a: nat) {x .y: nat} (ph: wff x): wff =
  $ A. y (y = a -> A. x (x = y -> ph)) $;
notation sb (a: nat) {x: nat} (ph: wff x): wff =
  ($[$:30) a ($/$:0) x ($]$:0) ph;

axiom ax_6 (a: nat) {x: nat}: $ E. x x = a $;
axiom ax_7 (a b c: nat): $ a = b -> a = c -> b = c $;

axiom ax_10 {x: nat} (ph: wff x): $ ~(A. x ph) -> A. x ~ (A. x ph) $;
axiom ax_11 {x y: nat} (ph: wff x y): $ A. x A. y ph -> A. y A. x ph $;
axiom ax_12 {x y: nat} (ph: wff y): $ A. y ph -> [ y / x ] ph $;

strict sort set;
term ab {x: nat} (ph: wff x): set;
notation ab {x: nat} (ph: wff x): set = (${$:max) x ($|$:0) ph ($}$:0);
term el: nat > set > wff; infixl el: $e.$ prec 50;
axiom elab (a: nat) {x: nat} (ph: wff x):
  $ a e. {x | ph} <-> [ a / x ] ph $;

def eqs (A B: set) {.x: nat}: wff = $ A. x (x e. A <-> x e. B) $;
infixl eqs: $==$ prec 50;

term the: set > nat;
axiom the_eq {x: nat} (a: nat): $ the {x | x = a} = a $;
axiom eqthe (A B: set): $ A == B -> the A = the B $;

def inter (A B: set) (.x: nat): set = $ {x | x e. A /\ x e. B} $;
infixl inter: $i^i$ prec 70;

def union (A B: set) (.x: nat): set = $ {x | x e. A \/ x e. B} $;
infixl union: $u.$ prec 65;

def univ (.x: nat): set = $ {x | T.} $; prefix univ: $V$ prec max;

def sbs (a: nat) {x .y: nat} (A: set x): set = $ {y | [ a / x ] y e. A} $;
notation sbs (a: nat) {x: nat} (A: set x): set =
  ($S[$:70) a ($/$:0) x ($]$:0) A;

def sbn (a: nat) {x .y: nat} (b: nat x): nat = $ the(S[ a / x ] {y | y = b}) $;
notation sbn (a: nat) {x: nat} (b: nat x): nat =
  ($N[$:70) a ($/$:0) x ($]$:0) b;

term d0: nat; prefix d0: $0$ prec max;
term suc: nat > nat;

def d1:  nat = $suc 0$; prefix d1:  $1$  prec max;
def d2:  nat = $suc 1$; prefix d2:  $2$  prec max;
def d3:  nat = $suc 2$; prefix d3:  $3$  prec max;
def d4:  nat = $suc 3$; prefix d4:  $4$  prec max;
def d5:  nat = $suc 4$; prefix d5:  $5$  prec max;
def d6:  nat = $suc 5$; prefix d6:  $6$  prec max;
def d7:  nat = $suc 6$; prefix d7:  $7$  prec max;
def d8:  nat = $suc 7$; prefix d8:  $8$  prec max;
def d9:  nat = $suc 8$; prefix d9:  $9$  prec max;
def d10: nat = $suc 9$; prefix d10: $10$ prec max;

axiom peano1 (a: nat): $ ~(0 = suc a) $;
axiom peano2 (a b: nat): $ suc a = suc b <-> a = b $;
axiom peano5 {x: nat} (ph: wff x):
  $ [ 0 / x ] ph -> A. x (ph -> [ suc x / x ] ph) -> A. x ph $;

term add: nat > nat > nat; infixl add: $+$ prec 65;
term mul: nat > nat > nat; infixl add: $*$ prec 70;

axiom add0 (a: nat): $ a + 0 = a $;
axiom addS (a b: nat): $ a + suc b = suc (a + b) $;
axiom mul0 (a: nat): $ a * 0 = 0 $;
axiom mulS (a b: nat): $ a * suc b = a * b + a $;

def le (a b .x: nat): wff = $ E. x a + x = b $;
infixl le: $<=$ prec 50;

def lt (a b: nat): wff = $ suc a <= b $;
infixl lt: $<$ prec 50;

def if (ph: wff) (a b: nat): nat;
theorem iftrue (ph: wff) (a b: nat): $ ph -> if ph a b = a $;
theorem iffalse (ph: wff) (a b: nat): $ ~ph -> if ph a b = b $;

def true (n: nat): wff = $ n != 0 $;
def bool (n: nat): wff = $ n < 2 $;
def nat (ph: wff): nat = $ if ph 1 0 $;

def pr (a b: nat): nat; infixr pr: $<>$ prec 54;
def fst (a: nat): nat;
def snd (a: nat): nat;

theorem fstpr (a b: nat): $ fst (a <> b) = a $;
theorem sndpr (a b: nat): $ snd (a <> b) = b $;
theorem fstsnd (a: nat): $ fst a <> snd a = a $;

theorem fst0: $ fst 0 = 0 $;
theorem snd0: $ snd 0 = 0 $;
theorem pr0: $ 0 <> 0 = 0 $;

def pi11 (n: nat): nat = $ fst (fst n) $;
def pi12 (n: nat): nat = $ snd (fst n) $;
def pi21 (n: nat): nat = $ fst (snd n) $;
def pi22 (n: nat): nat = $ snd (snd n) $;

def rec (z: nat) {x: nat} (s: nat x) (n: nat): nat;
theorem rec0 (z: nat) {x: nat} (s: nat x): $ rec z x s 0 = z $;
theorem recS (z: nat) {x: nat} (s: nat x) (n: nat):
  $ rec z x s (suc n) = N[ rec z x s n / x ] s $;

def sub (a b: nat): nat; infixl sub: $-$ prec 65;
theorem sub0 (a: nat): $ a - 0 = a $;
theorem subZ (b: nat): $ 0 - b = 0 $;
theorem subSS (a b: nat): $ suc a - suc b = a - b $;

def pow (a b: nat): nat; infixr pow: $^$ prec 80;
theorem pow0 (a: nat): $ a ^ 0 = 1 $;
theorem powS (a b: nat): $ a ^ suc b = a * a ^ b $;

def nel (a b: nat): wff;
def ns (a .x: nat): set = ${x | nel x a}$; coercion ns: nat > set;
theorem axext {x: nat} (a b: nat): $ a == b -> a = b $;
theorem extlt (a b: nat): $ a e. b -> a < b $;
theorem nel0 (a: nat): $ ~ a e. 0 $;

def lower (A: set) (.n: nat): nat = $ the {n | n == A} $;

def sn (a: nat): nat;
theorem elsn (a b: nat): $ a e. sn b <-> a = b $;

def ins (a b: nat): nat; infixr ins: $;$ prec 84;
theorem elins (a b c: nat): $ a e. ins b c <-> a = b \/ a e. c $;

def card (s: nat): nat;
theorem card0: $ card 0 = 0 $;
theorem cardS (a s: nat): $ ~a e. s -> card (a ; s) = suc (card s) $;

def cons (a b: nat): nat = $ suc (a <> b) $; infixr cons: $:$ prec 90;

def sep (n: nat) (A: set): nat;
theorem elsep (n: nat) (A: set) (a: nat):
  $ a e. sep n A <-> a e. n /\ a e. A $;

def range (n: nat): set;
theorem elrange (n m: nat): $ m e. range n <-> m < n $;

def isfun (A: set) (.a .b .b2: nat): wff =
$ A. a A. b A. b2 (a <> b e. A -> a <> b2 e. A -> b = b2) $;

def opab {x y .z: nat} (ph: wff x y): set =
$ {z | E. x E. y (z = x <> y /\ ph)} $;

def xp (A B: set) (.x .y .z: nat): set = $ opab x y (x e. A /\ y e. B) $;

def dom (A: set) (.x .y: nat): set = $ {x | E. y x <> y e. A} $;
def ran (A: set) (.x .y: nat): set = $ {y | E. x x <> y e. A} $;

def im (F A: set) (.x .y: nat): set = $ {y | E. x (x e. A /\ x <> y e. F)} $;
infixl im: $''$ prec 80;

def cnv (A: set) (.x .y: nat): set = $ opab x y (y <> x e. A) $;

def comp (F G: set) (.x .y .z: nat): set =
$ opab x z (E. y x <> y e. G /\ y <> z e. F) $;
infixr comp: $o.$ prec 90;

def res (A: set) (n: nat): nat = $ sep (lower (xp (range n) V)) A $;
infixl res: $|`$ prec 55;
theorem elres (n a b: nat) (A: set):
  $ isfun A -> (a <> b e. A |` n <-> a < n /\ a <> b e. A) $;

def lam {x .p: nat} (a: nat x): set = ${p | E. x p = x <> a}$;
notation lam {x: nat} (a: nat x): set = ($\$:30) x ($,$:0) a;

def app (F: set) (x .y: nat): nat = $ the {y | x <> y e. F} $;
infixl app: $@$ prec 200;

def srec {x: nat} (s: nat x): set;
theorem srecval {x y: nat} (s: nat x) (n: nat):
  $ srec x s @ n = N[ srec x s |` n / x ] s $;

def srecp (A: set): set;
theorem srecpval (A: set) (n: nat):
  $ n e. srecp A <-> n <> sep n (srecp A) e. A $;

def div (a b: nat): nat; infixl div: $//$ prec 70;
def mod (a b: nat): nat; infixl mod: $%$ prec 70;
theorem div0 (a: nat): $ a // 0 = 0 $;
theorem divmod (a b: nat): $ b * (a // b) + a % b = a $;
theorem modlt (a b: nat): $ b != 0 -> a % b < a $;

def b0 (n: nat): nat = $ n + n $;
def b1 (n: nat): nat = $ suc (b0 n) $;
def odd (n: nat): wff = $ n % 2 = 1 $;

def case (A B: set) (.n: nat): set =
  $ \ n, if (odd n) (B @ (n // 2)) (A @ (n // 2)) $;
theorem casel (A B: set) (n: nat): $ case A B @ (b0 n) = A @ n $;
theorem caser (A B: set) (n: nat): $ case A B @ (b1 n) = B @ n $;

def casep (A B: set): set;
theorem casepl (A B: set) (n: nat): $ b0 n e. casep A B <-> n e. A $;
theorem casepr (A B: set) (n: nat): $ b1 n e. casep A B <-> n e. B $;

def all (A: set) (n: nat): wff;
theorem all0 (A: set): $ all A 0 $;
theorem allS (A: set) (a b : nat): $ all A (a : b) <-> a e. A /\ all A b $;

def in (a l : nat): wff; infixl in: $IN$ prec 50;
theorem id0 (a: nat): $ ~ a IN 0 $;
theorem inS (a b l: nat): $ a IN (b : l) <-> a = b \/ a IN l $;

def lmems (l : nat): nat;
theorem ellmems (a l: nat): $ a e. lmems l <-> a IN l $;

def lrec (z: nat) (S: set) (n: nat): nat;
theorem lrec0 (z: nat) (S: set): $ lrec z S 0 = z $;
theorem lrecS (z: nat) (S: set) (a b: nat):
  $ lrec z S (a : b) = S @ (a <> lrec z S b) $;

def len (l: nat): nat;
theorem len0: $ len 0 = 0 $;
theorem lenS (a b: nat): $ len (a : b) = suc (len b) $;

def snoc (l a: nat): nat; infixl snoc: $|>$ prec 85;
theorem snoc0 (a: nat): $ 0 |> a = a : 0 $;
theorem snocS (a b c: nat): $ (a : b) |> c = a : (b |> c) $;
theorem snoclt (a b: nat): $ a < a |> b $;

def append (l1 l2: nat): nat; infixl append: $++$ prec 85;
theorem append0 (a: nat): $ 0 ++ a = a $;
theorem appendS (a b c: nat): $ (a : b) ++ c = a : (b ++ c) $;

def nth (n l: nat): nat;
theorem nth0 (n: nat): $ nth n 0 = 0 $;
theorem nthZ (a l: nat): $ nth 0 (a : l) = suc a $;
theorem nthS (n a l: nat): $ nth (suc n) (a : l) = nth n l $;
